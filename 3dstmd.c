/*
	3DSHax -- 3DS Hacking stuff
	3DS TMD reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file LICENSE or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

typedef struct {
	uint32_t cid;			// content id
	uint8_t hash[0x20];		// SHA-256 hash
} content_t;

typedef enum {
	RSA_2048_SHA256	= 0x00010004,
	RSA_4096_SHA256	= 0x00010003,
	RSA_2048_SHA1	= 0x00010001,
	RSA_4096_SHA1	= 0x00010000
} sigtype;

typedef struct {
        uint32_t sig_type;
        uint8_t *sig;
        uint8_t issuer[64];
        uint32_t tag;			// identifies what is being signed
        uint8_t name[64];		// name of thing being signed
        uint8_t key[0x104];
} certificate_t;

typedef struct {
	uint32_t sig_type; 
	uint8_t *sig;
	uint8_t fill1[60];
	uint8_t issuer[64];		// Root-CA%08x-CP%08x
	uint8_t version;
	uint8_t ca_crl_version;
	uint8_t signer_crl_version;
	uint8_t fill2;
	uint64_t sys_version;
	uint64_t title_id;
	uint32_t title_type;
	uint16_t group_id;		// publisher
	uint8_t reserved[62];
	uint32_t access_rights;
	uint16_t title_version;
	uint16_t num_contents;
	content_t contents[0x40];
	uint8_t padding[0x28];
	uint32_t boot_content;
	uint32_t banner_content;
	uint32_t banner_size;
	uint8_t hash[0x20]; /* Huh? */
	certificate_t *certs;
} tmd_t;

static char* sigtype_name[5] = {
	"RSA-2048 SHA-256", "RSA-4096 SHA-256", 
	"RSA-2048 SHA-1", "RSA-4096 SHA-1", "Unknown"
};

int sigtype_name_idx(sigtype type)
{
	switch(type) {
		case RSA_2048_SHA256: return 0;
		case RSA_4096_SHA256: return 1;
		case RSA_2048_SHA1: return 2;
		case RSA_4096_SHA1: return 3;
	}
	return 4;
}

void display_tmd(tmd_t* tmd)
{
	int i, l, end;
	printf("Signature type: %s\n", sigtype_name[sigtype_name_idx(tmd->sig_type)]);
	switch(tmd->sig_type) {
		case RSA_2048_SHA256:
		case RSA_2048_SHA1:
			end = 0x100;
			break;
		case RSA_4096_SHA256:
		case RSA_4096_SHA1:
			end = 0x200;
			break;
	}
	for(i = 0; i < end; i += 0x10)
		printf("	%02X %02X %02X %02X  %02X %02X %02X %02X   %02X %02X %02X %02X  %02X %02X %02X %02X\n",
			tmd->sig[i+0x0], tmd->sig[i+0x1], tmd->sig[i+0x2], tmd->sig[i+0x3],
			tmd->sig[i+0x4], tmd->sig[i+0x5], tmd->sig[i+0x6], tmd->sig[i+0x7],
			tmd->sig[i+0x8], tmd->sig[i+0x9], tmd->sig[i+0xA], tmd->sig[i+0xB],
			tmd->sig[i+0xC], tmd->sig[i+0xD], tmd->sig[i+0xE], tmd->sig[i+0xF]);
	printf("Issuer: %s\n", tmd->issuer);
	printf("TMD version: %d\n", tmd->version);
	printf("CA CRL version: %d\n", tmd->ca_crl_version);
	printf("Signer CRL version: %d\n", tmd->signer_crl_version);
	printf("System version: %08X-%08X\n", (uint32_t)(tmd->sys_version >> 32), (uint32_t)tmd->sys_version);
	printf("Title ID: %08X-%08X\n", (uint32_t)(tmd->title_id >> 32), (uint32_t)tmd->title_id);
	printf("Title type: 0x%08X\n", tmd->title_type);
	printf("Group ID: 0x%04X\n", tmd->group_id);
	printf("Access rights: 0x%08X\n", tmd->access_rights);
	printf("Title version: 0x%04X\n", tmd->title_version);
	printf("Number of contents: %d\n", tmd->num_contents);
	for(i = 0; i <= tmd->num_contents; i++) {
		printf("Content %d:\n", i);
		printf("	Content ID: %d\n", tmd->contents[i].cid);
		printf("	Hash:\n");
		for(l = 0; l < 0x20; l += 0x10)
			printf("		%02X %02X %02X %02X  %02X %02X %02X %02X   %02X %02X %02X %02X  %02X %02X %02X %02X\n",
				tmd->contents[i].hash[l+0x0], tmd->contents[i].hash[l+0x1], tmd->contents[i].hash[l+0x2], tmd->contents[i].hash[l+0x3],
				tmd->contents[i].hash[l+0x4], tmd->contents[i].hash[l+0x5], tmd->contents[i].hash[l+0x6], tmd->contents[i].hash[l+0x7],
				tmd->contents[i].hash[l+0x8], tmd->contents[i].hash[l+0x9], tmd->contents[i].hash[l+0xA], tmd->contents[i].hash[l+0xB],
				tmd->contents[i].hash[l+0xC], tmd->contents[i].hash[l+0xD], tmd->contents[i].hash[l+0xE], tmd->contents[i].hash[l+0xF]);
	}
	printf("Boot content: 0x%08X\n", tmd->boot_content);
	printf("Banner content: 0x%08X\n", tmd->banner_content);
	printf("Banner size: 0x%08X\n", tmd->banner_size);
	printf("Hash:\n");
	for(i = 0; i < 0x20; i += 0x10)
		printf("	%02X %02X %02X %02X  %02X %02X %02X %02X   %02X %02X %02X %02X  %02X %02X %02X %02X\n",
			tmd->hash[i+0x0], tmd->hash[i+0x1], tmd->hash[i+0x2], tmd->hash[i+0x3],
			tmd->hash[i+0x4], tmd->hash[i+0x5], tmd->hash[i+0x6], tmd->hash[i+0x7],
			tmd->hash[i+0x8], tmd->hash[i+0x9], tmd->hash[i+0xA], tmd->hash[i+0xB],
			tmd->hash[i+0xC], tmd->hash[i+0xD], tmd->hash[i+0xE], tmd->hash[i+0xF]);
}

#define ntohll(d) (((uint64_t)ntohl((d) & 0xFFFFFFFF) << 32) | (ntohl(((d) >> 32) & 0xFFFFFFFF)))
#define read8(filp) ({ uint8_t d; fread(&d, 1, 1, filp); d; })
#define read16(filp) ({ uint16_t d; fread(&d, 2, 1, filp); ntohs(d); })
#define read32(filp) ({ uint32_t d; fread(&d, 4, 1, filp); ntohl(d); })
#define read64(filp) ({ uint64_t d; fread(&d, 8, 1, filp); ntohll(d); })

content_t read_content(FILE* fp)
{
	content_t cnt;
	cnt.cid = read32(fp);
	fread(cnt.hash, 0x20, 1, fp);
	return cnt;
}

tmd_t* read_tmd(FILE* fp)
{
	tmd_t* tmd;
	int i;
	tmd = calloc(1, sizeof(tmd_t));
	if(tmd == NULL)
		return NULL;
	tmd->sig_type = read32(fp);
	switch(tmd->sig_type) {
		case RSA_2048_SHA256:
		case RSA_2048_SHA1:
			tmd->sig = malloc(0x100);
			if(tmd->sig == NULL)
				goto tmd_error1;
			fread(tmd->sig, 0x100, 1, fp);
			break;
		case RSA_4096_SHA256:
		case RSA_4096_SHA1:
			tmd->sig = malloc(0x200);
			if(tmd->sig == NULL)
				goto tmd_error1;
			fread(tmd->sig, 0x200, 1, fp);
			break;
	}
	fread(tmd->fill1, 60, 1, fp);
	fread(tmd->issuer, 64, 1, fp);
	tmd->version = read8(fp);
	tmd->ca_crl_version = read8(fp);
	tmd->signer_crl_version = read8(fp);
	tmd->fill2 = read8(fp);
	if(tmd->version != 1) {
		fprintf(stderr, "Not a 3DS TMD! TMD Version %d\n", tmd->version);
		goto tmd_error2;
	}
	tmd->sys_version = read64(fp);
	tmd->title_id = read64(fp);
	tmd->title_type = read32(fp);
	tmd->group_id = read16(fp);
	fread(tmd->reserved, 62, 1, fp);
	tmd->access_rights = read32(fp);
	tmd->title_version = read16(fp);
	tmd->num_contents = read16(fp);
	for(i = 0; i < 0x40; i++) {
		tmd->contents[i] = read_content(fp);
	}
	fread(tmd->padding, 0x28, 1, fp);
	tmd->boot_content = read32(fp);
	tmd->banner_content = read32(fp);
	tmd->banner_size = read32(fp);
	fread(tmd->hash, 0x20, 1, fp);
	/* TODO: Read Certs */
	return tmd;
tmd_error2:
	free(tmd->sig);
tmd_error1:
	free(tmd);
	return NULL;
}

void usage(char *app)
{
	fprintf(stderr, "Usage:\n"
			"	%s input.tmd\n", app);
}

int main(int argc, char *argv[])
{
	FILE* fp;
	tmd_t* tmd;
	fprintf(stderr, "3dstmd (c)2011 Alex Marshall \"trap15\" <trap15@raidenii.net>\n");
	if(argc < 2) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	fp = fopen(argv[1], "rb");
	if(fp == NULL) {
		perror("Couldn't open TMD file");
		return EXIT_FAILURE;
	}
	tmd = read_tmd(fp);
	if(tmd == NULL) {
		perror("TMD couldn't be read");
		return EXIT_FAILURE;
	}
	display_tmd(tmd);
	free(tmd);
	fclose(fp);
	return EXIT_SUCCESS;
}





