OBJECTS   = 3dstmd.o
OUTPUT    = 3dstmd
TARGETS   = $(OUTPUT)
CFLAGS    = -O0 $(INCLUDE)
LDFLAGS   = -g
CLEANED   = $(OBJECTS) $(TARGETS)

.PHONY: all clean

all: $(OBJECTS) $(TARGETS)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

3dstmd: 3dstmd.o
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@
clean:
	$(RM) $(CLEANED)

